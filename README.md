# CI Pipeline Sample Project

This project demonstrates a basic CI pipeline setup using GitLab CI/CD for a simple static website. The website is validated and deployed automatically using the provided `.gitlab-ci.yml` configuration file.

## Project Structure

```
ci-pipeline-sample/
├── .gitlab-ci.yml
└── public/
    └── index.html
```

## CI/CD Pipeline Configuration

The CI/CD pipeline is defined in the `.gitlab-ci.yml` file, which contains two main stages: `validate` and `deploy`.

### Stages

1. **Validate**
2. **Deploy**

### Validate Stage

In the `validate` stage, HTML validation is performed using the `cyb3rjak3/html5validator` Docker image. This stage ensures that the HTML files in the `public/` directory are valid and that the CSS is also checked.

```yaml
stages:
  - validate

html_validation:
  stage: validate
  image: cyb3rjak3/html5validator:latest
  script:
    - html5validator --root public/ --also-check-css --format text
```

### Deploy Stage

In the `deploy` stage, the website is deployed to GitLab Pages if the commit is on the default branch. The deployment uses the `alpine:latest` Docker image and copies the contents of the `public/` directory to the GitLab Pages artifacts.

```yaml
stages:
  - deploy

pages:
  stage: deploy
  image: alpine:latest
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - echo "Deploying to GitLab Pages"
    - ls -LR public/
  artifacts:
    paths:
      - public
```

## Public Directory

The `public/` directory contains the static HTML files for the website. The `index.html` file is the main entry point for the website.

### `index.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sample Website</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <h1>Welcome to My Sample Website</h1>
    <p>This is a simple static site hosted on GitLab Pages.</p>
    <p>Now this gets updated by pipeline</p>
</body>
</html>
```

## How to Use

1. **Clone the Repository**: Clone the project repository to your local machine.
2. **Modify HTML Files**: Edit the HTML files in the `public/` directory as needed.
3. **Commit and Push**: Commit your changes and push them to the repository.
4. **CI/CD Pipeline**: GitLab CI/CD will automatically run the pipeline, validate the HTML, and deploy the site to GitLab Pages if the changes are pushed to the default branch.

